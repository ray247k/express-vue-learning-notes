# Express Vue 學習筆記
使用 Express + Vue 寫一個迷你電商，資料庫使用 MongoDB 並打包成 Docker

參考 [图雀社区](https://tuture.co/) 發布的「从零到部署：用 Vue 和 Express 实现迷你全栈电商应用」系列文章進行練習。其中部分操作步驟因框架更新以及使用 ES6 的新寫法而有所差異，文字為自身整理後手打，並非直接使用翻譯軟體剪下貼上，用語部分也都換成臺灣慣用語。

專案版本庫位置：https://github.com/ray247k/mini-E-commerce

## 作者 : ray247k

## 聯絡資訊
|   項目  |            連結            |
| ------ | -------------------------- |
| Email  | ray247k@gmail.com          |
| GitHub | https://github.com/ray247k |
| 部落格  | https://tech.ray247k.com/  |
