# 初始設定
> ## 在這篇學到的事情：
> 使用 vue-ui 建立專案，並且安裝 vue-router
## 安裝所需要的環境
在這個練習中我們首先需要的是安裝 `npm` 以及`node`

這部分教學太多了，總之就先去裝起來吧！裝好後使用
```
npm -v
node -v
```
確定安裝成功了

接著要安裝我們前端所要使用的 vue 
```
npm install -g vue-cli
```
安裝完成後在 terminal 下指令確認已經成功安裝
```
vue --version
```

### 新建 vue 專案
這次嘗試使用 Vue-cli 新出的 [vue ui](https://cli.vuejs.org/zh/dev-guide/ui-api.html#ui-%E6%96%87%E4%BB%B6) 來建立專案，在終端機輸入
```
vue ui
```
就可以起動，根據提示我們開啟 vue-ui 的畫面
按下新增之後選擇指定的位置，根據畫面上提示來建立新專案

![vue-ui](../../static/01/01-vue-ui.png)

接著就是等待他執行完成，在終端機進到剛剛建立專案的資料夾下會發現一個 vue 專案已經被建立

根據 README 的說明執行以下指令後，可以打開 `http://localhost:8080/` 來預覽執行結果
```
npm install
npm run serve
```
![hello-vue](../../static/01/01-hello-vue.png)

#### 安裝 vue-router
接著還要安裝一個我們會用上的東西：[vue-router](https://router.vuejs.org/zh/)

可以選擇下指令，或是同樣的使用 vue-ui 來完成，既然剛剛使用 vue-ui 建立專案，這邊繼續使用 vue-ui 安裝 vue-router

打開 vue-ui 頁面，最上方搜尋欄的左邊看「新增 vue-router」給他按下去就會開始安裝了
![vue-router-install](../../static/01/01-vue-router-install.png)

安裝好之後可以進入 `main.js` 查看，友善的自動安裝已經幫我們引入了呢！

如此就完成了我們 vue 的初始化