# 連接 MongoDB

接著我們要安裝 mongoose 套件

[Mongoose](https://www.npmjs.com/package/mongoose): Mongoose 是一個 [MongoDB](https://www.mongodb.com/) 對象建模工具，用於在異步環境中工作。是時下最流行的 ODM（Object Document Mapping），要比直接操作底層 MongoDB Node 更方便
```
npm install mongoose
```


接著回到 `app.js` 中導入剛剛安裝的 `mongoose`
```js
const mongoose = require('mongoose')
```
並透過套件提供的方法連上我們的 MongoDB 資料庫
```js
mongoose.connect(`mongodb://localhost:27017/test`);
```

雖然現在看不出效果，但我們等等會嘗試操作資料庫來測試連線的有效性

但現在我們先做點額外的事情，儘管目前看來沒什麼用處但卻是必要的一環。那就是開啟跨來源資源共用 CORS