# 資料庫設計
先前我們安裝好了 mongoDB 以及開啟了 CORS，這一篇要來建立資料庫架構以及 MVC 架構中的 Model 部分

## Schema
在 Express 中，資料庫的 Schema 是建立在 `model/index.js` 中的，如果沒有 model 資料夾就自己建一個
```js
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const model = mongoose.model.bind(mongoose);
const ObjectId = mongoose.Schema.Types.ObjectId;

const productSchema = Schema({
  id: ObjectId,
  name: String,
  image: String,
  price: Number,
  description: String,
  manufacturer: { type: ObjectId, ref: 'Manufacturer' }
});

const manufacturerSchema = Schema({
  id: ObjectId,
  name: String,
});

const Product = model('Product', productSchema);
const Manufacturer = model('Manufacturer', manufacturerSchema);

module.exports = { Product, Manufacturer };

```

在下方兩個 model 的常數定義中 `Schema` 接收一個物件來定義資料結構以及對應的類型，除了常見的 `String`、`Number` 外，比較特別的是：`ObjectId` 代表 MongoDB 中資料的主鍵，具有唯一性。並且在 `productSchema` 中具有一個特別的屬性：`manufacturer`，這個物件定義了一個 `ref` 屬性，對應到 `Manufacturer` 這個 model 中。這是 MongoDB 的外部鍵值，與之對應的是 `Manufacturer` 中的 `ObjectId` 屬性的資料。這就是我們習慣的 MVC 架構中的「Model」部分。