# 建立 API

## 建立 Controller
接著就來建立兩個 Controller `productController` 和 `manufacturerController`

### controllers/manufacturer.js
這個 Controller 管理製造商（manufacturers）資料的操作，回頭看看上面路由中的方法是不是都找到了呢？

開頭導入了之前在 Model 中定義過的 `ManufacturerModel`，這是 mongoose 提供給我們操作資料庫的介面。通過 Model 上定義的一系列指令來對 Manufacturer 做資料操作

```js
const Model = require('../model');
const { Manufacturer } = Model;

const manufacturerController = {
  all(req, res) {
    Manufacturer.find({})
      .exec((err, manfacturers) => res.json(manfacturers))
  },
  byId(req, res) {
    const idParams = req.params.id;

    Manufacturer
      .findOne({ _id: idParams })
      .exec((err, manufacturer) => res.json(manufacturer));
  },
  create(req, res) {
    const requestBody = req.body;
    const newManufacturer = new Manufacturer(requestBody);

    newManufacturer.save((err, saved) => {
      Manufacturer
        .findOne({ _id: newManufacturer._id })
        .exec((err, manfacturer) => res.json(manfacturer))
    })
  },
  update(req, res) {
    const idParams = req.params.id;
    let manufacturer = req.body;

    Manufacturer.updateOne({ _id: idParams }, { ...manufacturer }, (err, updated) => {
      res.json(updated);
    })
  },
  remove(req, res) {
    const idParams = req.params.id;

    Manufacturer.findOne({ _id: idParams }).remove( (err, removed) => res.json(idParams) )
  }
}

module.exports = manufacturerController;

```

### controllers/product.js
而在 productController 中基本上與上方 manufacturerController 概念一致
```js
const Model = require('../model');
const { Product } = Model;

const productController = {
  all(req, res) {
    Product.find({})
      .populate('manufacturer')
      .exec((err, products) => res.json(products))
  },
  byId(req, res) {
    const idParams = req.params.id;

    Product
      .findOne({ _id: idParams })
      .populate('manufacturer')
      .exec((err, product) => res.json(product));
  },
  create(req, res) {
    const requestBody = req.body;
    const newProduct = new Product(requestBody);

    newProduct.save((err, saved) => {
      Product
        .findOne({ _id: newProduct._id })
        .populate('manufacturer')
        .exec((err, product) => res.json(product))
    })
  },
  update(req, res) {
    const idParams = req.params.id;
    const product = req.body;

    console.log('idParams', idParams);
    console.log('product', product);

    Product.updateOne({ _id: idParams }, { ...product }, (err, updated) => {
      res.json(updated);
    })
  },
  remove(req, res) {
    const idParams = req.params.id;

    Product.findOne({ _id: idParams }).remove( (err, removed) => res.json(idParams) )
  }
}

module.exports = productController;

```
完成後存擋，在終端機執行 `npm start`

先確定是你的 mongoDB 要是開著的，可以使用指令
```
mongo
```
看能不能進入 mongo 的指令列中，如果忘記怎麼開啟可以回到前面 [連接 MongoDB](/content/03/03_03_connect_mongoDB.md) 章節複習

## 測試 API
經過上面的流程，我們的 API 應該已經建好了，現在透過常用的 API 測試軟體 [Postman](https://www.postman.com/) 來測試

### manufacturers
- POST /api/v1/manufacturers

    首先先建立供應商的資料，測試新建一個供應商「Google」
    ![manufacturers-post](../../static/03/03-07-manufacturers-post.png)

- GET /api/v1/manufacturers

    查看供應商，因為我原本就有寫入了一個供應商「Apple」所以會有兩筆資料
    ![manufacturers-get](../../static/03/03-07-manufacturers-get.png)

- PUT /api/v1/manufacturers/:id

    接著更新「APPLE」為「Apple」
    ![manufacturers-put](../../static/03/03-07-manufacturers-put.png)

- DELETE /api/v1/manufacturers/:id

    最後我特別建立了一個叫做「samsung」的供應商用來測試刪除功能
    ![manufacturers-delete](../../static/03/03-07-manufacturers-delete.png)

### products
最後來測試新建產品資料
- POST /api/v1/products

    建立一個產品「iPhone 11」並且把他的供應商（manufacturer）指定給剛剛看過的「Apple」
    ![products-post](../../static/03/03-07-products-post.png)

- GET /api/v1/products
    新增完之後，使用 GET 方法取得資料查看會發現，iPhone 11 這筆資料的外部鍵值 `manufacturer` 屬性的確是對應到當初在 model 設定中指定的 manufacturer Model 的 `ObjectId` 資料內容。在這個範例中就是「Apple」。如此我們就建立了一款 Apple 出的手機：iPhone 11
    ![products-get](../../static/03/03-07-products-get.png)

## 結論
到這邊我們就已經建立好了 API 的服務

在這幾個章節中我們學到了
1. 了解 Express 路由以及如何使用 mongoose 連接 monogoDB 資料庫
2. 編寫路由、Controller 和 Model
3. 使用 Postman 測試 API

目前我們對於 Node 和 Express 搭建後端 API 有了基本的了解，就繼續前進吧！