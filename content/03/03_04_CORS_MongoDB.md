# 跨來源資源共用 CORS
CORS 是基於 [同源政策](https://developer.mozilla.org/zh-TW/docs/Web/Security/Same-origin_policy) 的限制，當關閉時限制了程式碼和不同網域資源間的互動。以就是說不同網域來的資源請求會被無情拒絕

讓我們回到 `app.js`，在剛剛的資料庫連線下一行加入以下程式碼
```js
// CORS config here
app.all('/*', function(req, res, next) {
  // CORS headers
  res.header("Access-Control-Allow-Origin", "*"); // restrict it to the required domain
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  // Set custom headers for CORS
  res.header('Access-Control-Allow-Headers', 'Content-type,Accept,X-Access-Token,X-Key');
  if (req.method == 'OPTIONS') {
    res.status(200).end();
  } else {
    next();
  }
});
```

通常來說會使用 npm 套件 [cors](https://www.npmjs.com/package/cors) 解決，但這邊先用簡單暴力的方式解決。未來如果有進行優化會再回頭安裝