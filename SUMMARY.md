# Summary

* [Express 學習筆記](README.md)

### 前端
* [初始設定](/content/01/01_introduction.md)
* [第一個 Vue 頁面](/content/02/02_01_first_vue_page.md)
    * [vue-route 的使用](/content/02/02_02_router_setting.md)

### 後端
* [認識 Express](/content/03/03_01_hello_express.md)
* [進入 MongoDB 的世界](/content/03/03_02_hello_mongoDB.md)
    * [連接 MongoDB](/content/03/03_03_connect_mongoDB.md)
    * [跨來源資源共用 CORS](/content/03/03_04_CORS_mongoDB.md)
    * [資料庫設計](/content/03/03_05_schemas_and_models.md)
    * [API 路由建立](/content/03/03_06_api_routers.md)
* [建立 API](/content/03/03_07_build_api.md)

### 資料串接
* [用 vue 建立一個表單子組件](/content/04/04_01_new_product_form.md)

### 資料流
* [認識 vuex](/content/05/05_01_install_vuex.md)
* [使用 Mutation 管理狀態](/content/05/05_02_mutation.md)
* [使用 Axios 取得 API 資料](/content/05/05_03_axios.md)

### 組件優化
* [使用組件思維重構頁面邏輯](/content/05/05_04_refactor_component.md)
* [使用 Vuex Getters 複用資料邏輯](/content/05/05_05_vuex_getter.md)
* [重構 Vuex store 中的邏輯](/content/05/05_06_refactor_vuex_store.md)
* [使用常數管理變數](/content/05/05_07_use_const.md)

### 套用組件庫 Element UI
* [產品列表套用](/content/06/06_01_element_ui_product_list.md)
* [重構頁面](/content/06/06_02_element_ui_refactor.md)
* [頁面功能恢復](/content/06/06_03_element_ui_fix.md)
* [加入載入動畫](/content/06/06_04_element_ui_loading.md)

### 進階內容
* [專案 Docker 容器化](/content/07/07_01_docker_container.md)